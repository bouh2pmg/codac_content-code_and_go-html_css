---
module:             C-COD-150
title:              HTML/CSS
subtitle:           Day 01

repository:         Pool_HTMLCSS_Day_01
repository rights:  ramassage-tek

author:             Enzo Nicoletti
version:            1.0

noFormalities:       true
noCleanRepo:         true
noBonusDir:          true
noErrorMess:         true

---
#newpage

# Foreword

Today, the exercises follow each other and are not independent. They are to be done in order.
&nbsp;

We advise you to copy/paste everything between two exercises before adding the requested functionalities.
&nbsp;

As a reminder, today's exercises will not be corrected automatically.
&nbsp;

All HTML pages must respect the basic HTML structure.

#newpage

# Exercise 01 ( 1pt )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_01/index.html
&nbsp;

Create a HTML file named **index.html**.
&nbsp;

Display "Welcome to CodingPedia" on that page.
&nbsp;

Create the pages **php.html** and **sql.html** for later use.

# Exercise 02 ( 1pt )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_02/index.html
&nbsp;

Insert the phrase "Welcome to CodingPedia" in a level 1 title tag.
&nbsp;

Create a paragraph describing the content of your site:
&nbsp;

"CodingPedia is a collective encyclopedia project on the Internet aiming to be universal and multi-lingual, and functions as a wiki. CodingPedia has for goal to offer freely reusable objective and verifiable content that anyone can modify and improve"
&nbsp;

Create a paragraph to present yourself:
&nbsp;

"I am 'login', currently a student at the coding-academy, and I love to stay in the pool all day long"

#newpage

# Exercise 03 ( 1pt )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_03/index.html 
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_03/php.html 
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_03/sql.html
&nbsp;

Create two links on your **index.html** page:

*  PHP
*  SQL

They will respectively redirect the user to:

*  **php.html**
*  **sql.html**

# Exercise 04 ( 1pt )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_04/index.html 
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_04/php.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_04/sql.html
&nbsp;

Use the following adequate structure tags on your three pages

*  A header tag
*  A nav tag
*  A main content tag
*  An aside content tag (this is a precise HTML tag that presents a content relative to the main content)
*  A footer tag (will contain an address tag with your coordinates) "'name' - 'first name' - 'address' - 'tel'"
&nbsp;
Each page of your site will display the same structure.

# Exercise 05 ( 1pt )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_05/index.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_05/php.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_05/sql.html
&nbsp;

Create a list on your index.html page that contains the links to the different pages (including index.html).
&nbsp;
Add this list, that will act as a menu, on all your HTML pages.

#newpage

# Exercise 06 ( 2pts )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_06/index.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_06/php.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_06/sql.html
&nbsp;
On your **php.html** and **sql.html** pages:
&nbsp;

Create 4 sections in your main content tag:

* The first section will contain a description of the page's technology. Use the HTML abbreviation tag.
    *  Copy - paste the first paragraph of Wikipedia.
    *  http://www.wikiwand.com/en/PHP
    *  http://www.wikiwand.com/en/SQL
*  A second empty section
*  The third will contain a list of your comments on this technology.
*  The fourth will contain code examples, use the appropriate tag.
&nbsp;

The aside content tag will contain a HTML citation tag:
&nbsp;

* "Peace is a lie, there is only passion. &nbsp;
    Through passion, I gain strength. &nbsp;
    Through strength, I gain power. &nbsp;
    Through power, I gain victory. &nbsp;
    Through victory, my chains are broken. &nbsp;
    The Force shall free me."

#newpage

# Exercise 07 ( 2pts )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_07/index.html 
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_07/php.html 
**File to turn in**:  Pool\_HTMLCSS\_Day\_01/ex\_07/sql.html 
&nbsp;

Come back to the second section of the preceding exercise.
&nbsp;

Insert a new array.
&nbsp;

Its title will be "Course planning".
&nbsp;

The array must contain in its header the columns "Subject", "Description" and "Grade obtained".
&nbsp;

Then you will answer these questions (using your imagination) on each line of the array for each day of the course.
&nbsp;

#newpage

# Exercise 08 ( 2pts )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_08/index.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_08/php.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_08/sql.html
&nbsp;

Create a form in a new section of the **index.html** page. This form will be used to ask questions on the technologies described in other pages.
&nbsp;

This form will contain the following fields:

* "Name" (required)
* "First name" (required)
* "Email" (required)
* "Age" (minimum 3, maximum 99 years old)
* "Phone" (10 characters)
* "Sex" (1 choice possible out of two, male by default)
* "Hair color" (you will choose the color with the help of a color selector)
* "Date" (today's date by default, required)
* "Technology" drop-down list of the technology in question
* "Question" (required and no size limit – you must use an appropriate element for this     kind of field, one that can contain a lot of text)
* A submit form button containing the text "Submit"
&nbsp;

Each field will be accompanied by its label.
&nbsp;

Each label will be associated to its field with an HTML tag.

# Exercise 09 ( 2pts )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_09/index.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_09/php.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_09/sql.html
&nbsp;

On each page (SQL and PHP) once again:
&nbsp;

Create a progress bar for yourself (this is a specific HTML element, and you are free to grade yourself as you wish) in a fifth section.
&nbsp;

Create a sorted list of subjects to review that you did not understand.

#newpage

# Exercise 10 ( 1pt )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_10/index.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_10/snow-white.css
&nbsp;

Create a CSS page named "snow\-white.css".
&nbsp;

Create a HTML index.html page and include your new CSS page.

# Exercise 11 ( 2pts )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_11/index.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_11/snow-white.css
&nbsp;

Add a little content to your HTML page so you can start formatting it.
&nbsp;

Create a paragraph containing the following text:
&nbsp;

"Once upon a time there lived a lovely princess with fair skin and blue eyes. She was so fair that she was named Snow White."
&nbsp;

Then create another paragraph containing:
&nbsp;

"One day, she was knitting and pricked herself. Three drops of blood fell in the snow."
&nbsp;

Thanks to a single CSS class named "a-great-style", apply to the two paragraphs a red color background and "Impact" as font.

#newpage

# Exercise 12 ( 2pts )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_12/index.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_12/snow-white.css
&nbsp;

Let's go further in the styling of your web page.
Make sure that:

*  The text size of the first paragraph is 21 pixels
*  The text size of the second paragraph is 27 pixels
*  The background color of the HTML page is "yellow"
*  The background color of the first paragraph is "black"
*  The background color of the of the second paragraph is "white"
*  The text color of the first paragraph is "white"
*  The text color of the second paragraph is "black"
*  The word "blood" in the second paragraph must have color "red" and be in italics. 

Italics is done with a CSS property. Formatting this word must not trigger a line feed. The value of CSS properties can be modified in the CSS file only.

# Exercise 13 ( 2pts )
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_13/index.html
**File to turn in**: Pool\_HTMLCSS\_Day\_01/ex\_13/snow-white.css
&nbsp;

Create a new class named "left-bordered-and-big" that will have the following consequences when applied to an element:

*  The element "floats" on the left
*  The element has a border of width 1 pixel, black and appears "solid"
*  The element has for width 60\% of the available space in its parent element\shortJump

Apply this class to the first paragraph of your page, without disabling the class applied in exercise 12.
&nbsp;

Create another class name "right-bordered-and-small" that will have the following consequences when applied to an element:

*  The element floats on the right
*  The element has a border of width 1 pixel, black and appears "solid"
*  The element has for width 20\% of the available space in its parent element\shortJump

Apply this class to the second paragraph of your page, without disabling the class applied in exercise 12.

#newpage

# Exercise BONUS

In this exercise, you will need the HTML pages made the previous day.

*  Get the pages HTML php.html and sql.html pages that you created up to exercise 7        (included) in the previous day.
*  Create a CSS page named "codingpedia.css".
*  Include this CSS page in your php.html and sql.html pages. 
*  Reproduce the following element layout without modifying your HTML pages.

#imageCenter(./img/codingpedia.png, 450px, 11)