---
module:             C-COD-150
title:              HTML/CSS
subtitle:           Day 02

repository:         Pool_HTMLCSS_Day_01
repository rights:  ramassage-tek

author:             Enzo Nicoletti
version:            1.0

noFormalities:       true
noCleanRepo:         true
noBonusDir:          true
noErrorMess:         true

---

#newpage

# Foreword

Today, the exercises follow each other and are not independent. They are to be done in order.
&nbsp;

We advise you to copy/paste everything between two exercises before adding the requested functionalities.
&nbsp;

As a reminder, today's exercises will not be corrected automatically.
&nbsp;

All HTML pages must respect the basic HTML structure.

#newpage

# Exercise 01 ( 4pts )
 **File to turn in**: PoolHTMLCSSDay_02/ex_01/index.html
 **File to turn in**: PoolHTMLCSSDay_02/ex_01/sql.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_01/codingpedia.css
&nbsp;

In this exercise, you will need the HTML pages made the previous day.

*  Get the pages HTML php.html and sql.html pages that you created up to exercise 7 (included) in the previous day.
*  Create a CSS page named **codingpedia.css**.
*  Include this CSS page in your php.html and sql.html pages.
*  Reproduce the following element layout without modifying your HTML pages

#imageCenter(./img/codingpedia.png, 300px)

#newpage

# Exercise 02 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_02/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_02/codingpedia.css
&nbsp;

For this exercise, you need the HTML index.html page created during the previous HTML day.

*  Get the index.html page that you had after exercise 8 of the previous day.
*  Include the "codingpedia.css" CSS file in your index.html page
*  Reproduce the following element layout without modifying your HTML page.Insert the phrase "Welcome to CodingPedia" in a level 1 title tag.

&nbsp;

#imageCenter(./img/codingpedia2.png, 400px)

#newpage

# Exercise 03 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_03/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_03/codingpedia.css
&nbsp;

#hint(You must adapt your site to make it responsive on each page.)

&nbsp;

*  Adapt your index.html page so that it has a resolution of 400*600px.

#imageCenter(./img/portrait.png, 300px)

*  Adapt your index.html page so that it has a resolution of 1280*720px in a new stylesheet.

#imageCenter(./img/landscape.png, 300px)

# Exercise 04 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_04/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_04/codingpedia.css
 **Restrictions**: Use of the function calc().
&nbsp;

In your **index.html** page, you must now integrate a "div" element that has a class "container".
&nbsp;

Upon display, the size of the container must be smaller than the total width of the page.
&nbsp;

You must apply a 20 pixels’ space on each side without using margins.
&nbsp;

Inside, you must implement a paragraph with the following text: "I have 40 pixels less in my container"

#newpage

# Exercise 05 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_05/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_05/codingpedia.css
&nbsp;

In your index.html page, you must now integrate a "div" element that has a class "container".
&nbsp;

In this container, you must add a new element with an "id" equal to "lin".
&nbsp;

This element must display a color gradient from light to dark blue in CSS only
&nbsp;

# Exercise 06 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_06/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_06/codingpedia.css
&nbsp;

#hint(selector)

&nbsp;

You must integrate a paragraph in your index.html page.
&nbsp;

You must select the first letter of the phrase in this paragraph and apply a grey background color to it.
&nbsp;

In addition, you must raise its font size to 24.
&nbsp;

#newpage

# Exercise 07 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_07/index.html&nbsp;
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_07/codingpedia.css
&nbsp;

In your **index.html** page, you must now integrate a "div" element that has a class "container".
&nbsp;

You must then implement 3 elements in the container with a class named "block".
&nbsp;

Two of them must have an attribute named foobar. One of these attributes will have value "foo1" and the other "foo2".
&nbsp;

The "block" class must represent a green square with 50 pixels’ sides.
&nbsp;

The element having attribute "foo1" must have a red background and the one with attribute "foo2" must have a blue background.

# Exercise 08 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_08/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_08/codingpedia.css
&nbsp;

You must integrate a paragraph containing the phrase "I am an uncommon font".
&nbsp;

You must apply the "Satisfy" font to this paragraph

#newpage

# Exercise 09 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_09/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_09/codingpedia.css

&nbsp;
In your **index.html** page, you must now integrate a "div" element that has a class "container".
&nbsp;

In your index.html page, you must now integrate a "div" element that has a class "container".
&nbsp;

In this element, introduce another element that has a "mybutton" class. In this element with class "mybutton", insert the text "Send".
&nbsp;

This element must have a yellow border of 3 pixels’ width and rounded angles.

# Exercise 10 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_10/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_10/codingpedia.css
&nbsp;

In your **index.html** page, you must now integrate a "div" element that has a class "container".
&nbsp;

In this element, add an element of class "mybutton".
&nbsp;

In the "mybutton" element, add the text "Send".
&nbsp;

This text will have a border made with the border.png image. This asset will be given to you.

# Exercise 11 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_11/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_11/codingpedia.css
&nbsp;

In your index.html page, you must now integrate a "div" element that has a class "container".
&nbsp;

This div must contain a paragraph with the following text: "I am a customized text".
&nbsp;

This text will have a shadow.

# Exercise 12 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_12/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_12/codingpedia.css
&nbsp;

#hint(box-sizing)

&nbsp;

In your **index.html** page, you must now integrate a "div" element that has a class "container".
&nbsp;


In this element, you must display three other square-shaped elements with an "inline-block" display type.
&nbsp;

Each of these elements will be a square with 120 pixels’ sides and be aligned to their top side.
&nbsp;

The first element will have id "block1" with an orange background color.
&nbsp;

The second element will have id "block2" with a white background color, a 10 px yellow border and an 8 pixels padding
&nbsp;

The third element will have id "block3" with a black background color and a 32 px padding.
&nbsp;

These three elements must have the same size when they are displayed.

# Exercise 13 ( 1pts )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_13/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_13/codingpedia.css
&nbsp;

In your **index.html** page, you must now integrate a "div" element that has a class "container".
&nbsp;

This element must be 500 px wide.
&nbsp;

In addition, this element must contain a paragraph in which a [lorem ipsum](http://www.faux-texte.com/loremipsum-15.htm) will be displayed in three columns (in CSS only).

# Exercise 14 ( 4pts )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_14/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_14/codingpedia.css
&nbsp;

In your index.html page, you must now integrate a "div" element that has a class "container".
You must implement two images in the background. 
&nbsp;

Both images must be visible. 
&nbsp;

The first image paper.gif must cover all of the container and the second image flower.gif must be positioned in the
bottom right corner of the container. 
&nbsp;

These assets will be given to you.

# Exercise 15 ( 1pt )
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_15/index.html
 **File to turn in**: Pool_HTMLCSS_Day_02/ex_15/codingpedia.css
&nbsp;

In your index.html page, you must now integrate a "div" element that has a class "container".
&nbsp;

In this element, you will add a bulleted list containing an element of your choice.
&nbsp;

By using a CSS "pseudo class", you will add "Element: " before each "li" element and "end of sentence" after each "li"
element.


# Exercise 16 ( 2 pts )
 **File to turn in**: Pool_HTMLCSS_Day02/ex16/index.html
 **File to turn in**: Pool_HTMLCSS_Day02/ex16/codingpedia.css
&nbsp;

In your index.html page, you must now integrate a "div" element that has a class "container".
&nbsp;

This element will represent a red, 100 * 100 px square.
&nbsp;

You must now make the square move.
&nbsp;

At first, the square must move 300 pixels to the bottom and take on a green color.
&nbsp;

Then, the square will move 300 pixel to the right and take on a blue color.
&nbsp;

Then, the square will move up 300 pixel and take on a yellow color.
&nbsp;

Finally, the square returns to its starting position and its color goes back to red.
&nbsp;

This movement will be performed 5 times before stopping. Each cycle must last 5 seconds.