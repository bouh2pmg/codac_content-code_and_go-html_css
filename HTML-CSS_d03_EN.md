---
module:             C-COD-150
title:              HTML/CSS
subtitle:           Day 03

repository:         Pool_HTMLCSS_Day_03
repository rights:  ramassage-tek

author:             Enzo Nicoletti
version:            1.0

noCleanRepo:         true
noBonusDir:          true
noErrorMess:         true

---
#newpage

# Foreword

Today, you will be introduced to CSS frameworks
&nbsp;

Look for responsive design as it will be valued as a Bonus
&nbsp;

All HTML pages must respect the basic HTML structure.
&nbsp;

# Subject

Using the materialize CSS framework you need to reproduce one of the following mock-ups.

*  http://materializecss.com/
*  https://github.com/Dogfalo/materialize

#newpage

# Batman
&nbsp;

#imageCenter(./img/batman.png, 500px, 11)

# Licorne
&nbsp;

#imageCenter(./img/licorne.png, 500px, 11)